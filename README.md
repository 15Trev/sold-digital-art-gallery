Welcome to the repository for Sold Digital Art Gallery. Over the course of two weeks, three team members and I created this full stack, user friendly web site that allows artists and art dealers to publish and sell their art online.

![alt text](https://bitbucket.org/15Trev/sold-digital-art-gallery/raw/80ee476de3501b511aea0c43cd2d814a99438b6d/Screenshots/Sold%20Screenshot%201.png)

![alt text](https://bitbucket.org/15Trev/sold-digital-art-gallery/raw/80ee476de3501b511aea0c43cd2d814a99438b6d/Screenshots/Sold%20Screenshot%202.png)

Manager view:

![alt text](https://bitbucket.org/15Trev/sold-digital-art-gallery/raw/80ee476de3501b511aea0c43cd2d814a99438b6d/Screenshots/Sold%20Screenshot%203.png)

Artist/Dealer view:

![alt text](https://bitbucket.org/15Trev/sold-digital-art-gallery/raw/80ee476de3501b511aea0c43cd2d814a99438b6d/Screenshots/Sold%20Screenshot%204.png)

![alt text](https://bitbucket.org/15Trev/sold-digital-art-gallery/raw/80ee476de3501b511aea0c43cd2d814a99438b6d/Screenshots/Sold%20Screenshot%205.png)

![alt text](https://bitbucket.org/15Trev/sold-digital-art-gallery/raw/80ee476de3501b511aea0c43cd2d814a99438b6d/Screenshots/Sold%20Screenshot%206.png)